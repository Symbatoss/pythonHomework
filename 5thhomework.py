people = [
    {
        'name': 'Arkadyi',
        'age': 20
    },

    {
        'name': 'Diana',
        'age': 17
    },

    {
        'name': 'Kanykei',
        'age': 18
    },
    {
        'name': 'Anton',
        'age': 15
    }
]

def filterMass(people):
    newMass = []
    for person in people:
        if person["age"] >= 18:
            newMass.append(person)
    return newMass


newFilteredMass = filterMass(people)

def newKey(people):
    for person in people:
        person["checked"] = True

    return people

addedMass = newKey(newFilteredMass)

def changedKeys(people):
    for person in people:
        person["checked"] = False
    return people

changedMass = changedKeys(addedMass)

def vyvod(people):
    for person in people:
        print(person)


vyvod(changedMass)