# Напишите функцию которая принемает *args
def summa(*args):
    print(args)
    print(sum(args))


# summa(64, 66, 87)
nums = [55, 66, 66]
summa(*nums)
print()


# Напишите функцию которая принемает *kwargs
def second(**kwargs):
    print(kwargs)


second(first='i have no idea', second='welcome!')
print()


# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
def solve(*args):
    s = 1
    for something in args:
        s *= something
    print(s)
solve(8, 9)
solve(7, 10, 5)
print()
# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор),
# но с использованием параметров args, kwargs
girls = [
    {
        'name': 'Ayana',
        'age': 15
    },
    {
        'name': 'Aidana',
        'age': 19
    },
    {
       'name': 'Deniza',
        'age': 23
    },
]
boys = [
    {
        'name': 'Anton',
        'age': 19
    },
    {
        'name':'Andrew',
        'age': 26
    },
    {
        'name': 'Sultan',
        'age': 17
    },
]

def clubWork(*args, **kwargs):
    for i in args:
        for people in i:
            if people['age'] >= 18:
                print(f"{kwargs['secure']}: {people['name']} проходите!")
                print(f"{kwargs['admin']}: Поставила печать {people['name']}\n")
            else:
                print(f"{kwargs['secure']}: {people['name']} вам нельзя!\n")


clubWork(girls, boys, admin="Symbat", secure="Daniels")