# Напишите приммеры использования всех операций со словарями
delete = {
    'name': 'Anton',
    'age': 19
          }
delete.clear()
print('deleted =', delete)
print()
    # copy()
copy = {
    'life is good!',
            }
copied_sentence = copy.copy()
print('sentence which should copy:', copy)
print('print copied sentence:', copied_sentence)
print()
    # fromkeys()
numbers = {'one', 'two', 'three', 'four', 'five'}
value = {'number'}
number = dict.fromkeys(numbers, value)
print(number)
print()
    # get()
computer = {
    "brand": "Acer",
    "model": "e1-410",
    "year": 2020
}
print_informaion = computer.get("brand")
print(print_informaion)
print()
    # items()
person = {
  "name": "Ademi",
  "age": "10",
  "grade": 4
}

person_information = person.items()

print(person_information)
print()
    # keys()
person = {
  "name": "Ademi",
  "age": "10",
  "grade": 4
}

person_keys = person.keys()

print(person_keys)
print()
    # values()
vegetable = {
    'cucumber': 22,
    'potato': 53,
    'carrot': 34,
    'cabbage': 35,
}
vegetable_values = vegetable.values()
print("vegetable values", vegetable_values)
print()
    # pop()
vegetable = {
    'cucumber': 22,
    'potato': 53,
    'apple': 42,
    'carrot': 34,
    'cabbage': 35,
            }
vegetable.pop('apple')
print(vegetable)
print()
    # popitem()
person = {
  "name": "Ademi",
  "age": "10",
  "grade": 4
}
person.popitem()
print(person)
print()

# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
    #clear()
clear_dict = {
    'name': 'Anton',
    'age': 19
          }
print('original dict:', clear_dict)
def udalit(clear_dict):
    print('deleted dict =', clear_dict)
clear_dict.clear()
udalit(clear_dict)
print()
    # copy()
copy_dict = {
    'banana': 33,
    'apple': 45
}
print('original dict:', copy_dict)
def kopirovanie(copy_dict):
    print('copied dict:', copy_dict)
copy_dict.copy()
kopirovanie(copy_dict)
print()
    # fromkeys()
fromkeys_dict = {'five', '88', 'fourty three'}
value1 = 'number'
print('original fromkeys dict', fromkeys_dict)
def dobavit(fromkeys_dict):
    number1 = dict.fromkeys(fromkeys_dict, value1)
    print('new fromkeys dict', number1)
dobavit(fromkeys_dict)
print()
    # get()
get_dict = {
    'cucumber': 22,
    'potato': 53,
    'carrot': 34,
    'cabbage': 35,
}
print('original get dict:', get_dict)
def naiti(get_dict):
    get = get_dict.get('potato')
    print('new get dict:', get)
naiti(get_dict)
print()
    # items()
items_dict = {
  "name": "Ademi",
  "age": "10",
  "grade": 4
}
print('original items dict:', items_dict)
def elementy(items_dict):
    item = items_dict.items()
    print('new items dict:', item)
elementy(items_dict)
print()
    # keys()
dict_keys = {
    "brand": "Acer",
    "model": "e1-410",
    "year": 2020
}
print('original dict:', dict_keys)
def key(dict_keys):
    kei = dict_keys.keys()
    print('new dict:', kei)
key(dict_keys)
print()
    # values()
dict_values = {
  "name": "Ademi",
  "age": "10",
  "grade": 4
}
print('original dict:', dict_values)
def value(dict_values):
    val = dict_values.values()
    print('dict values:', val)
value(dict_values)
print()
    # pop()
pop_dict = {
    'cucumber': 22,
    'potato': 53,
    'carrot': 34,
    'cabbage': 35,
}
print('original dict:', pop_dict)
def udalenie(pop_dict):
    pop_dict.pop('cabbage')
    print('new changed dict:', pop_dict)
udalenie(pop_dict)
print()
    # popitem()
popitem_dict = {
    'name': 'Anton',
    'hobby': 'sing',
    'age': 19,
          }
print('original dict', popitem_dict)
def last(popitem_dict):
    popitem_dict.popitem()
    print('cnanged dict:', popitem_dict)
last(popitem_dict)
print()
# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()
# --- > Здесь опишите решение
#dict.update()
dict_update = {
    "name": "Ademi",
     "age": "10",
    "grade": 4
}
print('orirginal dict', dict_update)
dict_update.update({'hobby': 'dance'})
print('updated dict', dict_update)
print()
#dict.setdefault()
dict_setdefault = {
    "brand": "Acer",
    "model": "e1-410",
    "year": 2020
}
print('original dict:', dict_setdefault)
print('find brand:', dict_setdefault.setdefault('brand'))
print()
# Напишите пример вложенной функции.
# --- > Здесь опишите решение
def function1():
    a = 'Me too'

    def function2():
        a = 'I love programming'

        print(a)

    function2()
    print(a)
function1()
print()
# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую запись в файле
array = ['world', 'forest', 'nature']
def  create(array):
    file = open('first.txt', 'w')
    file.write(str(array))
    file.close()
create(array)
# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую файл новыми записями
array2 = ['world', 'forest', 'nature']
def  addnew(array2):
    file = open('second.txt', 'w')
    file.write(str(array2) + 'hello')
    file.close()
addnew(array2)
# Напишите функцию считывающую данные из файла
file = open('second.txt', 'r')
for i in file:
    print(i)
    print()
# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает только отфильтрованные данные
peopl = [{
    'name': 'Anton',
     'age': 19,
 },
 {
     'name': 'Aidai',
     'age': 17,
 }]
def filterAndWrite(peopl):
    file = open('third.txt', 'w')
    for pers in peopl:
        if pers['age'] > 18:
            file.write(str(pers))

    file.close()


filtred = filterAndWrite(peopl)